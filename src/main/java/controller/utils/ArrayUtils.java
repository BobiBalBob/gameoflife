package controller.utils;

import model.Cell;

public class ArrayUtils {

    public static Cell[][] splitByRowsAndColumns(Cell[][] base, int rowMin, int rowMax, int columnMin, int columnMax) {
        rowMin = rowMin < 0 ? 0 : rowMin;
        rowMax = rowMax >= base.length ? base.length - 1 : rowMax;
        columnMin = columnMin < 0 ? 0 : columnMin;
        columnMax = columnMax >= base.length ? base[0].length - 1 : columnMax;
        Cell[][] result = new Cell[rowMax - rowMin + 1][columnMax - columnMin + 1];
        int currentRow = 0;
        int currentColumn = 0;
        for(int row= rowMin ;row <= rowMax ; row++) {
            for (int column = columnMin; column <= columnMax; column++) {
                result[currentRow][currentColumn] = base[row][column];
                currentColumn++;
            }
            currentColumn = 0;
            currentRow++;
        }

        return result;

    }
}
