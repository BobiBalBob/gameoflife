package controller;

import model.Cell;
import model.ComputingArea;
import model.ObservableArea;
import model.SplittedArea;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;

public class CellsEvolutionThread implements RunnableFuture {
    private ObservableArea observableArea;
    private SplittedArea result;
    private boolean done;

    public CellsEvolutionThread(ObservableArea observableArea) {
        this.observableArea = observableArea;
    }

    @Override
    public void run() {
        boolean areaUpdated = false;
        Cell[][] cells = observableArea.getSplittedArea().getCells();

        List<CellEvolutionThread> cellEvolutionThreads= new ArrayList<>();
        ComputingArea computingArea = AreaOptimizator.getComputingArea(observableArea.getObservableArea().getCells());
        for(int row = computingArea.getRowMin(); row <= computingArea.getRowMax(); row++) {
            for (int column = computingArea.getColumnMin(); column <= computingArea.getColumnMax(); column++) {
                if(!isInMargin(observableArea, row, column)) {
                    int currentRow = row;
                    int currentColumn = column;
                    Cell currentCell = observableArea.getObservableArea().getCells()[row][column];
                    Cell[] neighborhood = new Cell[8];
                    neighborhood[0] = getCell(currentRow, currentColumn, -1, -1, observableArea.getObservableArea().getCells());
                    neighborhood[1] = getCell(currentRow, currentColumn, -1, 0, observableArea.getObservableArea().getCells());
                    neighborhood[2] = getCell(currentRow, currentColumn, -1, 1, observableArea.getObservableArea().getCells());

                    //Ligne courante
                    neighborhood[3] = getCell(currentRow, currentColumn, 0, -1, observableArea.getObservableArea().getCells());
                    neighborhood[4] = getCell(currentRow, currentColumn, 0, 1, observableArea.getObservableArea().getCells());

                    //ligne après
                    neighborhood[5] = getCell(currentRow, currentColumn, 1, -1, observableArea.getObservableArea().getCells());
                    neighborhood[6] = getCell(currentRow, currentColumn, 1, 0, observableArea.getObservableArea().getCells());
                    neighborhood[7] = getCell(currentRow, currentColumn, 1, 1, observableArea.getObservableArea().getCells());
                    CellEvolutionThread cellEvolutionThread = new CellEvolutionThread(currentCell, currentRow - observableArea.getMinRowWithModificator(), currentColumn - observableArea.getMinColumnWithModificator(), neighborhood);
                    cellEvolutionThreads.add(cellEvolutionThread);
                    cellEvolutionThread.run();
                }
            }
        }
        for(CellEvolutionThread cellEvolutionThread : cellEvolutionThreads) {
            Cell cell = cellEvolutionThread.get();
            cells[cellEvolutionThread.getCurrentRow()][cellEvolutionThread.getCurrentColumn()] = cell;
            if(cell.isAlive() != cell.isAliveNextTurn()) {
                areaUpdated = true;
            }
        }
        result = new SplittedArea(cells, observableArea.getSplittedRowMin(), observableArea.getSplittedRowMax(), observableArea.getSplittedColumnMin(), observableArea.getSplittedColumnMax());
        result.setAreaUpdated(areaUpdated);
        done = true;
    }

    public boolean isInMargin(ObservableArea observableArea, int row, int column){
        if(observableArea.isMarginLeft() && column == 0) {
            return true;
        } else if(observableArea.isMarginTop() && row == 0) {
            return true;
        } else if(observableArea.isMarginRight() && (column == observableArea.getColumnSize() - 1)) {
            return true;
        } else return observableArea.isMarginBottom() && (row == observableArea.getRowSize() - 1);
    }

    private static Cell getCell(int row, int column, int rowModificator, int columnModificator, Cell[][] cells) {
        int rowOffsetted = row + rowModificator;
        int columnOffsetted = column + columnModificator;
        if(rowOffsetted < 0 || columnOffsetted < 0 || rowOffsetted >= cells.length || columnOffsetted >= cells[0].length) {
            return null;
        }

        return cells[rowOffsetted][columnOffsetted];
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        //NOP, we don't want stop
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return done;
    }

    @Override
    public SplittedArea get() {
        while(!done) {
            //On attends que ça finisse
        }
        return result;
    }

    @Override
    public SplittedArea get(long timeout, TimeUnit unit) {
        while(!done) {
            //On attends que ça finisse
        }
        return result;
    }
}
