package controller;

import controller.utils.ArrayUtils;
import model.Cell;
import model.ComputingArea;
import model.ObservableArea;
import model.SplittedArea;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;

public class LifeMechanic implements RunnableFuture {
    private Cell[][] oldGeneration;
    private List<SplittedArea> result;
    private boolean done;

    public LifeMechanic(Cell[][] oldGeneration) {
        this.oldGeneration = oldGeneration;
    }


    @Override
    public void run() {
        result = goToNextGeneration();
        done = true;

    }

    private List<SplittedArea> goToNextGeneration() {
        List<ObservableArea> oldGenerationSplitted = splitBoard(oldGeneration);
        List<CellsEvolutionThread> cellsEvolutionThreads = new ArrayList<>();
        for(ObservableArea newGeneration : oldGenerationSplitted) {
            CellsEvolutionThread cellsEvolutionThread = new CellsEvolutionThread(newGeneration);
            cellsEvolutionThreads.add(cellsEvolutionThread);
            cellsEvolutionThread.run();
        }
        List<SplittedArea> newGenerations = new ArrayList<>();
        for(CellsEvolutionThread cellsEvolutionThread : cellsEvolutionThreads) {
            SplittedArea splittedArea = cellsEvolutionThread.get();
            newGenerations.add(splittedArea);
        }

        return newGenerations;
    }

    /**
     * Split the board in 2 steps :
     *  -> get the smallest area containing all alive cells
     *  -> split them into X area of the size of SplittedArea.AREA_SIZE
     *
     *  We have to keep a margin when it's possible to not falsify results after
     * @param cells
     * @return
     */
    private List<ObservableArea> splitBoard(Cell[][] cells) {
        List<ObservableArea> splitBoard = new ArrayList<>();
        ComputingArea computingArea = AreaOptimizator.getComputingArea(oldGeneration);
        int optimazedRowMax ,optimazedColumnMax, optimazedRowMin, optimazedColumnMin;
        //Si les dimensions sont inférieures à la zone souhaité, on place rowMax à (rowMin + AREA_SIZE) (idem pour column)
        optimazedRowMax = computingArea.getRowMax() - computingArea.getRowMin() < SplittedArea.AREA_SIZE ? computingArea.getRowMin() + SplittedArea.AREA_SIZE - 1: computingArea.getRowMax();
        optimazedColumnMax = computingArea.getColumnMax() - computingArea.getColumnMin() < SplittedArea.AREA_SIZE ? computingArea.getColumnMin() + SplittedArea.AREA_SIZE - 1 : computingArea.getColumnMax();
        optimazedRowMin = computingArea.getRowMin();
        optimazedColumnMin = computingArea.getColumnMin();
        //Si suite à ça, on dépasse les dimensions du tableau, on ajuste le max et on recule le min d'autant
        if(optimazedRowMax >= cells.length) {
            optimazedRowMin = optimazedRowMin  - (optimazedRowMax - cells.length - 1);
            optimazedRowMax = cells.length - 1;
        }
        if(optimazedColumnMax >= cells[0].length) {
            optimazedColumnMin = optimazedColumnMin  - (optimazedColumnMax - cells[0].length - 1);
            optimazedColumnMax = cells[0].length - 1;
        }

        Cell[][] cellsoptimzed = ArrayUtils.splitByRowsAndColumns(cells, optimazedRowMin, optimazedRowMax, optimazedColumnMin, optimazedColumnMax);

        double columnSize = Math.ceil(cellsoptimzed[0].length / Double.valueOf(SplittedArea.AREA_SIZE));
        double rowSize = Math.ceil(cellsoptimzed.length / Double.valueOf(SplittedArea.AREA_SIZE));
        for(int i = 0; i < rowSize; i++) {
            for(int j = 0; j < columnSize; j++) {
                splitBoard.add(getObservableArea(cells,
                        optimazedRowMin + i * SplittedArea.AREA_SIZE,
                        Math.min(optimazedRowMin + i * SplittedArea.AREA_SIZE + (SplittedArea.AREA_SIZE - 1), cells.length - 1),
                        optimazedColumnMin + j * SplittedArea.AREA_SIZE,
                        Math.min(optimazedColumnMin+ j * SplittedArea.AREA_SIZE + (SplittedArea.AREA_SIZE - 1), cells[0].length - 1)
                ));
            }
        }

        return splitBoard;
    }

    /**
     * This split a 2D array to get a part of him trying to keep margin of 1 cell
     * @param totalCells
     * @param rowMin
     * @param rowMax
     * @param columnMin
     * @param columnMax
     * @return
     */
    private ObservableArea getObservableArea(Cell[][] totalCells, int rowMin, int rowMax, int columnMin, int columnMax) {
        int maxIndexe = totalCells.length-1;
        rowMax = rowMax > maxIndexe ? maxIndexe : rowMax;
        columnMax = columnMax > maxIndexe ? maxIndexe : columnMax;

        int observableRowMin = rowMin - 1 < 0 ? 0 : rowMin - 1;
        boolean marginTop = rowMin > observableRowMin;
        int observableColumnMin = columnMin - 1 < 0 ? 0 : columnMin - 1;
        boolean marginLeft = columnMin > observableColumnMin;
        int observableRowMax = rowMax + 1 > maxIndexe ? maxIndexe : rowMax + 1;
        boolean marginBottom = rowMax < observableRowMax;
        int observableColumnMax = columnMax + 1 > maxIndexe ? maxIndexe : columnMax + 1;
        boolean marginRight = columnMax < observableColumnMax;

        Cell[][] cellsSplittedArea = ArrayUtils.splitByRowsAndColumns(totalCells, observableRowMin,observableRowMax, observableColumnMin, observableColumnMax);

        SplittedArea observableArea = new SplittedArea(cellsSplittedArea, observableRowMin, observableRowMax, observableColumnMin, observableColumnMax);
        return new ObservableArea(observableArea, marginLeft, marginTop, marginRight, marginBottom);
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        //NOP, we don't want stop
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return done;
    }

    @Override
    public List<SplittedArea> get() {
        while(!done) {
            //On attends que ça finisse
        }
        return result;
    }

    @Override
    public List<SplittedArea> get(long timeout, TimeUnit unit) {
        while(!done) {
            //On attends que ça finisse
        }
        return result;
    }
}
