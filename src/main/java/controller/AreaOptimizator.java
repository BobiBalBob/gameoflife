package controller;

import model.Cell;
import model.ComputingArea;

public class AreaOptimizator {

    public static ComputingArea getComputingArea(Cell[][] cells) {
        int rowNumber = cells.length;
        int columnNumber = cells[0].length;
        int rowMin = lookArray(rowNumber, columnNumber, cells, true);
        int rowMax = lookReverseArray(rowNumber, columnNumber, cells, true);
        int columnMin = lookArray(columnNumber, rowNumber, cells, false);
        int columnMax = lookReverseArray(columnNumber, rowNumber, cells, false);

        rowMin = rowMin < 0 ? 0 : rowMin;
        columnMin = columnMin < 0 ? 0 : columnMin;
        rowMax = rowMax > rowNumber - 2 ? rowNumber -2 : rowMax;
        columnMax = columnMax > columnNumber - 2 ? columnNumber - 2 : columnMax;

        return new ComputingArea(columnMin, rowMin, columnMax, rowMax);

    }

    private static int lookArray(int iMax, int jMax, Cell[][] cells, boolean isRow){
        for(int i = 0; i < iMax; i++) {
            for(int j = 0; j < jMax; j++) {
                if(isRow && cells[i][j].isAlive() || !isRow && cells[j][i].isAlive()) {
                    return i - 1 < 0 ? 0 : i - 1;
                }
            }
        }
        return iMax/2;
    }

    private static int lookReverseArray(int iMax, int jMax, Cell[][] cells, boolean isRow){
        for(int i = iMax -1 ; i >= 0; i--) {
            for(int j = 0; j < jMax; j++) {
                if(isRow && cells[i][j].isAlive() || !isRow && cells[j][i].isAlive()) {
                    return i + 1;
                }
            }
        }
        return iMax/2;
    }
}
