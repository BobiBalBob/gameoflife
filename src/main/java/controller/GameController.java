package controller;

import javafx.application.Platform;
import model.Cell;
import model.GameConfiguration;
import model.SetupFactory;
import model.SplittedArea;
import view.Window;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class GameController {
    private Window window;
    private int generation = 0;
    private boolean started = false;
    private int cellNumber;
    private int speed;
    private int selectionSize;

    public GameController(Window window) {
        this.window = window;
        setCellNumber(GameConfiguration.DEFAULT_SIZE);
        setSpeed(GameConfiguration.DEFAULT_SPEED);
        setSelectionSize(GameConfiguration.SELECTION_SIZE);
    }

    public void initBoard(Boolean[][] boardSetup) {
        Thread thread = new Thread(() -> {
            generation = 0;
            Runnable updateBoardUI = () -> window.setCellSetup(boardSetup);
            updateUI(updateBoardUI);
            Runnable updateGenerationLabelUI = () -> window.updateGenerationLabel(generation);
            updateUI(updateGenerationLabelUI);
        });
        thread.start();
    }

    public void startGame() {
        Thread thread = new Thread(() -> {
            started = true;
            long startTime = System.currentTimeMillis();

            while(started) {
                LifeMechanic lifeMechanic = new LifeMechanic(window.getBoard().getCells());
                lifeMechanic.run();
                List<SplittedArea> splittedAreas = lifeMechanic.get();
                Cell[][] nextGen = joinSplittedAreas(splittedAreas);
                window.replaceCell(nextGen);
                generation++;

                //List<SplittedArea> splittedAreasUpdated = splittedAreas.stream().filter(current -> current.isAreaUpdated()).collect(Collectors.toList());
                Runnable updaterUI = () ->  window.updateUI(generation);
                updateUI(updaterUI);

                if(generation == 100) {
                    long endTime = System.currentTimeMillis();

                    System.out.println("That loop took  " + (endTime - startTime)  + " milliseconds");
                }
                doPause();
            }

        });
        thread.start();
    }

    private Cell[][] joinSplittedAreas(List<SplittedArea> splittedAreas) {
        Cell[][] cells = window.getBoard().getCells().clone();

        for(SplittedArea splittedArea : splittedAreas) {
            for(int row = 0; row < splittedArea.getCells().length; row++) {
                for(int column = 0; column < splittedArea.getCells()[0].length; column++) {
                    cells[splittedArea.getMinRow() + row][splittedArea.getMinColumn() + column] = splittedArea.getCell(row, column);
                }
            }
        }
        return cells;
    }

    public void stopGame() {
        started = false;
    }

    private void updateUI(Runnable updaterUI) {
        CountDownLatch doneLatch = new CountDownLatch(1);
        Platform.runLater(() -> {
            try {
                updaterUI.run();
            } finally {
                doneLatch.countDown();
            }
        });
        try {
            doneLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setCellNumber(int cellNumber) {
        this.cellNumber = cellNumber;
        window.setCellSize(BigDecimal.valueOf(GameConfiguration.WINDOWS_SIZE / getSizeAsDouble())
                .setScale(2, RoundingMode.HALF_UP)
                .doubleValue());
        initBoard(SetupFactory.getInitialClearSetup(cellNumber));
    }

    private void doPause() {
        try {
            Thread.sleep(801 - speed);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean isStarted() {
        return started;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    private double getSizeAsDouble() {
        return cellNumber;
    }

    public int getCellNumber() {
        return cellNumber;
    }


    public int getSelectionSize() {
        return selectionSize;
    }

    public void setSelectionSize(int selectionSize) {
        this.selectionSize = selectionSize;
    }
}
