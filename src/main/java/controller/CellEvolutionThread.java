package controller;

import model.Cell;

import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;

public class CellEvolutionThread implements RunnableFuture {
    private boolean done = false;
    private Cell currentCell;
    private int currentRow;
    private int currentColumn;
    private Cell[] neighborhood;

    public CellEvolutionThread(Cell currentCell, int currentRow, int currentColumn, Cell[] neighborhood) {
        this.currentCell = currentCell;
        this.currentRow = currentRow;
        this.currentColumn = currentColumn;
        this.neighborhood = neighborhood;
    }

    @Override
    public void run() {
        currentCell.setAliveNextTurn(willBeAlive());
        done = true;
    }


    private boolean willBeAlive() {
        boolean isAliveNow = currentCell.isAlive();

        int totalAlive = 0;

        for(int cellNumber = 0; cellNumber < 8; cellNumber++) {
            if(neighborhood[cellNumber] != null
               && neighborhood[cellNumber].isAlive()) {
                totalAlive++;
            }
        }

        if(totalAlive == 3) {
            return true;
        }
        if(totalAlive == 2) {
            return isAliveNow;
        }
        return false;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        //NOP, we don't want stop
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return done;
    }

    @Override
    public Cell get() {
        while(!done) {
            //Wait for ending
        }
        return currentCell;
    }

    public int getCurrentRow() {
        return currentRow;
    }

    public int getCurrentColumn() {
        return currentColumn;
    }

    @Override
    public Cell get(long timeout, TimeUnit unit) {
        while(!done) {
            //Wait for ending
        }
        return currentCell;
    }
}
