package model;

public class Cell {

    private boolean isAlive;
    private boolean isAliveNextTurn;
    public Cell(boolean isAlive) {
        this.isAlive = isAlive;
        this.isAliveNextTurn = isAlive;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAliveNextTurn(boolean aliveNextTurn) {
        isAliveNextTurn = aliveNextTurn;
    }

    public boolean isAliveNextTurn() {
        return isAliveNextTurn;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }
}
