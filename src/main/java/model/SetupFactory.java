package model;

import java.util.HashMap;
import java.util.Map;

public class SetupFactory {
    public static final String CANON_SETUP_NAME = "Canon";
    public static final String CLEAR_SETUP_NAME = "Clear";
    public static final String PULSAR_SETUP_NAME = "Pulsar";
    public static final String EXOTIC_ONE_SETUP_NAME = "Exotique";

    public static Map<String, Boolean[][]> getSetupAsMap(){
        Map<String, Boolean[][]> map = new HashMap<>();
        map.put(CLEAR_SETUP_NAME, getInitialClearSetup(GameConfiguration.DEFAULT_SIZE));
        map.put(CANON_SETUP_NAME, getCanon());
        map.put(PULSAR_SETUP_NAME, getPulsar());
        map.put(EXOTIC_ONE_SETUP_NAME, getExoticOne());

        return map;
    }

    public static Boolean[][] getInitialClearSetup(int currentSize) {
        Boolean[][] initCells = new Boolean[currentSize][currentSize];

        return initCells;
    }

    public static Boolean[][] getCanon() {
        Boolean[][] canonCells = new Boolean[40][40];
        canonCells[11][25] = true;
        canonCells[12][23] = true;
        canonCells[12][25] = true;
        canonCells[13][13] = true;
        canonCells[13][14] = true;
        canonCells[13][21] = true;
        canonCells[13][22] = true;
        canonCells[13][35] = true;
        canonCells[13][36] = true;
        canonCells[14][12] = true;
        canonCells[14][16] = true;
        canonCells[14][21] = true;
        canonCells[14][22] = true;
        canonCells[14][35] = true;
        canonCells[14][36] = true;
        canonCells[15][1] = true;
        canonCells[15][2] = true;
        canonCells[15][11] = true;
        canonCells[15][17] = true;
        canonCells[15][21] = true;
        canonCells[15][22] = true;
        canonCells[16][1] = true;
        canonCells[16][2] = true;
        canonCells[16][11] = true;
        canonCells[16][15] = true;
        canonCells[16][17] = true;
        canonCells[16][18] = true;
        canonCells[16][23] = true;
        canonCells[16][25] = true;
        canonCells[17][11] = true;
        canonCells[17][17] = true;
        canonCells[17][25] = true;
        canonCells[18][12] = true;
        canonCells[18][16] = true;
        canonCells[19][13] = true;
        canonCells[19][14] = true;

        return canonCells;
    }

    public static Boolean[][] getPulsar() {
        Boolean[][] pulsarCells = new Boolean[2][5];
        pulsarCells[0][0] = true;
        pulsarCells[0][1] = true;
        pulsarCells[0][2] = true;
        pulsarCells[0][3] = true;
        pulsarCells[0][4] = true;
        pulsarCells[1][0] = true;
        pulsarCells[1][4] = true;


        return pulsarCells;
    }

    public static Boolean[][] getExoticOne() {
        Boolean[][] exoticOneCells = new Boolean[50][50];
        exoticOneCells[15][36] = true;
        exoticOneCells[16][36] = true;
        exoticOneCells[17][34] = true;
        exoticOneCells[17][46] = true;
        exoticOneCells[17][47] = true;
        exoticOneCells[18][23] = true;
        exoticOneCells[18][24] = true;
        exoticOneCells[18][31] = true;
        exoticOneCells[18][33] = true;
        exoticOneCells[18][46] = true;
        exoticOneCells[18][47] = true;
        exoticOneCells[19][22] = true;
        exoticOneCells[19][26] = true;
        exoticOneCells[19][31] = true;
        exoticOneCells[19][33] = true;
        exoticOneCells[20][12] = true;
        exoticOneCells[20][13] = true;
        exoticOneCells[20][21] = true;
        exoticOneCells[20][27] = true;
        exoticOneCells[20][31] = true;
        exoticOneCells[20][33] = true;
        exoticOneCells[21][12] = true;
        exoticOneCells[21][13] = true;
        exoticOneCells[21][21] = true;
        exoticOneCells[21][25] = true;
        exoticOneCells[21][27] = true;
        exoticOneCells[21][28] = true;
        exoticOneCells[21][34] = true;
        exoticOneCells[21][36] = true;
        exoticOneCells[22][21] = true;
        exoticOneCells[22][27] = true;
        exoticOneCells[22][36] = true;
        exoticOneCells[23][22] = true;
        exoticOneCells[23][26] = true;
        exoticOneCells[24][23] = true;
        exoticOneCells[24][24] = true;

        return exoticOneCells;
    }
}
