package model;

public class ComputingArea {
    private int columnMin, rowMin, columnMax, rowMax;

    public ComputingArea(int columnMin, int rowMin, int columnMax, int rowMax) {
        this.columnMin = columnMin;
        this.rowMin = rowMin;
        this.columnMax = columnMax;
        this.rowMax = rowMax;
    }

    public int getColumnMin() {
        return columnMin;
    }

    public void setColumnMin(int columnMin) {
        this.columnMin = columnMin;
    }

    public int getRowMin() {
        return rowMin;
    }

    public void setRowMin(int rowMin) {
        this.rowMin = rowMin;
    }

    public int getColumnMax() {
        return columnMax;
    }

    public void setColumnMax(int columnMax) {
        this.columnMax = columnMax;
    }

    public int getRowMax() {
        return rowMax;
    }

    public void setRowMax(int rowMax) {
        this.rowMax = rowMax;
    }
}
