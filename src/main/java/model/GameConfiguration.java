package model;

import java.util.Arrays;
import java.util.List;

public class GameConfiguration {
    public static final int WINDOWS_SIZE = 650;
    public static final int WINDOWS_MARGIN = 45;
    public static final int SELECTION_SIZE = 1;
    public static final int DEFAULT_SIZE = 15;
    public static final int DEFAULT_SPEED = 300;
    public static final int BUTTON_SIZE = 120;
    public static final String START_BUTTON_LABEL = "Start";
    public static final String STOP_BUTTON_LABEL = "Stop";
    public static final String EXTRACT_BUTTON_LABEL = "Extraire setup";
    public static final String GENERATION_LABEL = "Génération n° : ";
    public static final List<Integer> SIZES_AVAILABLE = Arrays.asList(10, 15, 20, 40, 50, 60, 70, 80, 90, 100, 200, 300, 500, 1000);
    public static final List<Integer> SPEED_AVAILABLE = Arrays.asList(300, 500, 700, 750, 780, 800);
    public static final List<Integer> SELECTION_SIZE_AVAILABLE = Arrays.asList(1, 2, 5, 10);

}
