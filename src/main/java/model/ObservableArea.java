package model;

/**
 * This is an area to analyse
 * It contains the splitted area (part of the array to analyse) more a margin when it's possible
 */
public class ObservableArea {
    /**
     * This is the area to analyse more the margin
     */
    private SplittedArea observableArea;
    /**
     * Margin indications
     */
    boolean marginLeft;
    boolean marginTop;
    boolean marginRight;
    boolean marginBottom;

    public ObservableArea(SplittedArea observableArea, boolean marginLeft, boolean marginTop, boolean marginRight, boolean marginBottom) {
        this.observableArea = observableArea;
        this.marginLeft = marginLeft;
        this.marginTop = marginTop;
        this.marginRight = marginRight;
        this.marginBottom = marginBottom;
    }

    //------------------------ Splitted datas ---------------------------
    public SplittedArea getSplittedArea() {
        Cell[][] splitArea = new Cell[getSplittedRowSize()][getSplittedColumnSize()];
        int currentRow = 0;
        int currentColumn = 0;
        for(int row= getMinRowWithModificator() ;row <= getMaxRowWithModificator() ; row++) {
            for (int column = getMinColumnWithModificator(); column <= getMaxColumnWithModificator(); column++) {
                splitArea[currentRow][currentColumn] = observableArea.getCells()[row][column];
                currentColumn++;
            }
            currentColumn = 0;
            currentRow++;
        }

        return new SplittedArea(splitArea, observableArea.getMinRow(), observableArea.getMaxRow(), observableArea.getMinColumn(), observableArea.getMaxColumn());
    }

    public int getSplittedRowMax() {
        return observableArea.getMaxRow() - (isMarginBottom() ? 1 : 0);
    }

    public int getSplittedRowMin() {
        return observableArea.getMinRow() + (isMarginTop() ? 1 : 0);
    }

    public int getSplittedColumnMax() {
        return observableArea.getMaxColumn() - (isMarginRight() ? 1 : 0);
    }

    public int getSplittedColumnMin() {
        return observableArea.getMinColumn() + (isMarginLeft() ? 1 : 0);
    }

    public int getSplittedColumnSize() {
        return getSplittedColumnMax() - getSplittedColumnMin() + 1;
    }

    public int getSplittedRowSize() {
        return getSplittedRowMax() - getSplittedRowMin() + 1;
    }

    public int getMinRowWithModificator() {
        return (isMarginTop() ? 1 : 0);
    }

    public int getMinColumnWithModificator() {
        return (isMarginLeft() ? 1 : 0);
    }

    public int getMaxRowWithModificator() {
        return (isMarginBottom() ? observableArea.getCells().length - 2 : observableArea.getCells().length - 1);
    }

    public int getMaxColumnWithModificator() {
        return (isMarginRight() ? observableArea.getCells()[0].length - 2 : observableArea.getCells()[0].length - 1);
    }

    //------------------------ Observable datas ---------------------------

    public int getColumnSize() {
        return getObservableArea().getCells()[0].length;
    }

    public int getRowSize() {
        return getObservableArea().getCells().length;
    }

    public SplittedArea getObservableArea() {
        return observableArea;
    }

    public boolean isMarginLeft() {
        return marginLeft;
    }
    public boolean isMarginTop() {
        return marginTop;
    }

    public boolean isMarginRight() {
        return marginRight;
    }

    public boolean isMarginBottom() {
        return marginBottom;
    }
}
