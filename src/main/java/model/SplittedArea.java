package model;

/**
 * This is an array of cells who is a part of one bigger (the parent)
 */
public class SplittedArea {
    public static final int AREA_SIZE = 10;

    private Cell[][] cells;
    /**
     * (row,column) coordinates (min and max)
     * to locate in parent
     */
    private int minRow;
    private int maxRow;
    private int minColumn;
    private int maxColumn;
    private boolean isAreaUpdated = false;

    public SplittedArea(Cell[][] cells, int minRow, int maxRow, int minColumn, int maxColumn) {
        this.cells = cells;
        this.minRow = minRow;
        this.maxRow = maxRow;
        this.minColumn = minColumn;
        this.maxColumn = maxColumn;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public Cell getCell(int row, int column) {
        return cells[row][column];
    }
    public int getMinRow() {
        return minRow;
    }

    public int getMaxRow() {
        return maxRow;
    }

    public int getMinColumn() {
        return minColumn;
    }

    public int getMaxColumn() {
        return maxColumn;
    }

    public boolean isAreaUpdated() {
        return isAreaUpdated;
    }

    public void setAreaUpdated(boolean areaUpdated) {
        isAreaUpdated = areaUpdated;
    }

    public SplittedArea clone() {
        return new SplittedArea(cells, minRow, maxRow, minColumn, maxColumn);
    }
}
