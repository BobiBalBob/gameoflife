package model;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ExtractSetupDialogModel {
    public static final String TITLE = "Setup création";
    public static final String HEADER_TEXT = "Création du nouveau setup";
    public static final String CONTENT_TEXT = "Quel sera son nom ?";
    public static final String TITLE2 = "Positions sélectionnées";
    public static final String HEADER_TEXT2 = "Code à copier/coller :";

    public static String generateCodeAsString(String nameSetup, Boolean[][] cells) {
        if(nameSetup == null || nameSetup.isEmpty()) {
            nameSetup = "empty";
        }
        StringBuilder message = new StringBuilder();
        int size = cells.length;
        String[] nameSetupSplitted = nameSetup.trim().split("\\s");
        String camelCaseNameFirstUpper = Arrays.stream(nameSetupSplitted)
                .map(current -> {
                    if(current.length() <= 1) {
                        return current.toUpperCase();
                    } else {
                        return current.substring(0, 1).toUpperCase() + current.substring(1).toLowerCase();
                    }
                })
                .collect(Collectors.joining());
        String camelCaseName = camelCaseNameFirstUpper.substring(0, 1).toLowerCase() + camelCaseNameFirstUpper.substring(1);
        message.append("public static Boolean[][] get");
        message.append(camelCaseNameFirstUpper);
        message.append("() {\n");
        message.append("    Boolean[][] " + camelCaseName + "Cells = new Boolean[" + size + "][" + size +"];\n");
        for(int row = 0; row < size; row++) {
            for (int column = 0; column < size; column++) {
                if(cells[row][column]) {
                    message.append("    " + camelCaseName + "Cells[" + row + "]["+column + "] = true;\n");
                }
            }
        }
        message.append("    return " +camelCaseName + "Cells;\n");
        message.append("}\n");

        return message.toString();
    }
}
