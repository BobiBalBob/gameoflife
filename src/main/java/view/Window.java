package view;

import controller.GameController;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import model.Cell;
import model.GameConfiguration;
import view.board.Board;
import view.menu.GlobalMenu;

public class Window extends GridPane {
    private GameController gameController;
    private GlobalMenu globalMenu;
    private Board board;
    private double cellSize;

    public Window() {
        gameController = new GameController(this);
        board = new Board(this);
        globalMenu = new GlobalMenu(this);
        setAlignment(Pos.CENTER);
        setMinWidth(getResizedBoardSize()+ GameConfiguration.WINDOWS_MARGIN);

        this.add(globalMenu, 0, 0);
        this.add(board, 0 , 1);
    }

    public void updateUI(int generation) {
        getBoard().updateCellsColors();
        updateGenerationLabel(generation);
    }

    public void updateGenerationLabel(int generation) {
        globalMenu.updateGenerationLabel(generation);
    }

    public void setCellSetup(Boolean[][] boardSetup) {
        int rowLength = boardSetup.length;
        int columnLength = boardSetup[0].length;
        int rowOffset = (gameController.getCellNumber() - rowLength)/2;
        int columnOffset = (gameController.getCellNumber() - columnLength)/2;

        board.setCells(new Cell[gameController.getCellNumber()][gameController.getCellNumber()]);
        for(int row = 0; row < gameController.getCellNumber(); row++) {
            for (int column = 0; column < gameController.getCellNumber(); column++) {
                int rowOffsetted = row - rowOffset;
                int columnOffsetted = column - columnOffset;
                Cell cell;
                if(rowOffsetted <0 || columnOffsetted <0
                    || rowOffsetted >= rowLength || columnOffsetted >= columnLength
                    ||boardSetup[rowOffsetted] == null || boardSetup[rowOffsetted][columnOffsetted] == null) {
                    cell = new Cell(false);
                } else {
                    cell = new Cell(boardSetup[rowOffsetted][columnOffsetted]);
                }
                board.setCell(cell, row, column);
            }
        }
        board.updateCellsColors();
    }

    public void replaceCell(Cell[][] cells) {
        for(int row = 0; row < gameController.getCellNumber(); row++) {
            for (int column = 0; column < gameController.getCellNumber(); column++) {
                board.setCell(cells[row][column], row, column);
            }
        }
    }

    public int getCellNumber() {
        return gameController.getCellNumber();
    }

    public double getCellSize() {
        return  cellSize;
    }

    public void setCellSize(double cellSize) {
        this.cellSize = cellSize;
    }

    public double getResizedBoardSize() {
        return getCellNumber()*getCellSize();
    }

    public Board getBoard() {
        return board;
    }

    public void playStop() {
        if(!gameController.isStarted()) {
            startGame();
        } else {
            stopGame();
        }
    }

    public void setBoardSize(int boardSize) {
        stopGame();
        gameController.setCellNumber(boardSize);
    }

    public void setSpeed(int speed) {
        gameController.setSpeed(speed);
    }

    public int getSpeed() {
        return gameController.getSpeed();
    }

    public int getSelectionSize() {
        return gameController.getSelectionSize();
    }

    public void setSelectionSize(int selectionSize) {
        gameController.setSelectionSize(selectionSize);
    }

    public void loadSetup(Boolean[][] setup) {
        if(!gameController.isStarted()) {
            gameController.initBoard(setup);
        }
    }

    private void stopGame() {
        gameController.stopGame();
        globalMenu.stopGame();
        board.disableCellsClick(false);
    }

    private void startGame() {
        gameController.startGame();
        globalMenu.startGame();
        board.disableCellsClick(true);
    }
}
