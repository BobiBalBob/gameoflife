package view.board;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.Cell;
import view.Window;
import view.menu.listeners.CellClickListener;
import view.menu.listeners.CellDragListener;

import java.util.ArrayList;
import java.util.List;

public class Board extends Canvas {

    private Cell[][] cells;
    private Window window;
    private Cell currentOverCell = null;
    private GraphicsContext gc;

    public Board(Window window) {
        super(window.getResizedBoardSize(), window.getResizedBoardSize());
        this.cells = new Cell[window.getCellNumber()][window.getCellNumber()];
        this.window = window;

        this.setOnMouseClicked(new CellClickListener(this));
        this.setOnMouseDragged(new CellDragListener(this));
    }


    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    public void updateCellsColors() {
        Color black = Color.BLACK;
        gc = getGraphicsContext2D();
        gc.clearRect(0, 0, getWidth(), getHeight());

        // vertical lines
        double strokeSize = window.getCellSize()/100.0;
        if(strokeSize > 0.05) {
            gc.setStroke(black);
            gc.setLineWidth(strokeSize);
            for(double i = 0 ; i <= window.getResizedBoardSize() ; i+=window.getCellSize()){
                gc.strokeLine(i, 0, i, window.getResizedBoardSize());
            }
            for(double i = 0 ; i <= window.getResizedBoardSize() ; i+=window.getCellSize()){
                gc.strokeLine(0, i, window.getResizedBoardSize(), i);
            }
        }

        int currentRow = 0;
        int currentColumn = 0;
        for(double y = 0; y < window.getResizedBoardSize(); y += window.getCellSize()) {
            for(double x = 0; x < window.getResizedBoardSize(); x += window.getCellSize()) {
                Cell currentCell = cells[currentRow >= window.getCellNumber() ? window.getCellNumber() - 1 : currentRow]
                                        [currentColumn >= window.getCellNumber() ? window.getCellNumber() - 1 : currentColumn];
                if(currentCell.isAliveNextTurn()) {
                    gc.setFill(black);
                    gc.fillRect(x, y, window.getCellSize(), window.getCellSize());
                }
                currentCell.setAlive(currentCell.isAliveNextTurn());
                currentColumn++;
            }
            currentColumn = 0;
            currentRow++;
        }
    }

    public Cell[][] getCells() {
        return cells;
    }

    public void setCell(Cell cell, int row, int column) {
        cells[row][column] = cell;
    }

    public void disableCellsClick(boolean disable) {
        if(disable) {
            setOnMouseClicked(null);
            setOnMouseDragged(null);
        } else {
            setOnMouseClicked(null);
            setOnMouseDragged(null);
            setOnMouseClicked(new CellClickListener(this));
            setOnMouseDragged(new CellDragListener(this));
        }
    }

    public void clickByPosition(double x, double y) {
        Cell clickedCell = getCellAtPosition(x, y);
        currentOverCell = clickedCell;
        clickedCell.setAliveNextTurn(!clickedCell.isAlive());
        updateCellsColors();

    }

    public void dragOnPosition(double x, double y) {
        Cell clickedCell = getCellAtPosition(x, y);
        if(clickedCell != null && clickedCell != currentOverCell) {
            currentOverCell = clickedCell;
            List<Cell> clickedCells = getCellsAtPosition(x, y);
            clickedCells.stream().forEach(cell -> {
                cell.setAliveNextTurn(true);
                cell.setAlive(false);
            });
            updateCellsColors();
        }

    }


    public Cell getCellAtPosition(double x, double y) {
        if(x < 0 || y < 0) {
            return null;
        }
        int row = (int) (y/window.getCellSize());
        int column = (int)(x/window.getCellSize());
        row = row >= window.getCellNumber() ? window.getCellNumber() - 1 : row;
        column = column >= window.getCellNumber() ? window.getCellNumber() - 1 : column;
        return cells[row][column];
    }

    public List<Cell> getCellsAtPosition (double x, double y){
        List<Cell> clickedCells = new ArrayList<>();
        for(int i = 0; i< window.getSelectionSize(); i++) {
            for(int j = 0; j< window.getSelectionSize(); j++) {
                clickedCells.add(getCellAtPosition(x + i * window.getCellSize(), y + j * window.getCellSize()));
            }
        }
        return clickedCells;
    }
}
