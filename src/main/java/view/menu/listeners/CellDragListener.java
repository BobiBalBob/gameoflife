package view.menu.listeners;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import view.board.Board;

public class CellDragListener implements EventHandler<MouseEvent> {

    private Board board;

    public CellDragListener(Board board) {
        this.board = board;
    }

    @Override
    public void handle(MouseEvent dragEvent) {
        board.dragOnPosition(dragEvent.getX(), dragEvent.getY());
    }
}
