package view.menu.listeners;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import view.board.Board;

public class CellClickListener implements EventHandler<MouseEvent> {

    private Board board;

    public CellClickListener(Board board) {
        this.board = board;
    }

    @Override
    public void handle(MouseEvent dragEvent) {
        board.clickByPosition(dragEvent.getX(), dragEvent.getY());
    }
}
