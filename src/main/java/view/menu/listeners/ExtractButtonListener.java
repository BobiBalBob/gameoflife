package view.menu.listeners;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;
import model.ExtractSetupDialogModel;
import view.Window;

import java.util.Arrays;
import java.util.Optional;

public class ExtractButtonListener implements EventHandler<ActionEvent> {
    private Window window;

    public ExtractButtonListener(Window window) {
        this.window = window;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        doExtract();
    }

    public void doExtract() {
        String newSetupName = "RENAME_ME";
        TextInputDialog dialog = new TextInputDialog(newSetupName);
        dialog.setTitle(ExtractSetupDialogModel.TITLE);
        dialog.setHeaderText(ExtractSetupDialogModel.HEADER_TEXT);
        dialog.setContentText(ExtractSetupDialogModel.CONTENT_TEXT);

        Optional<String> result = dialog.showAndWait();

        if (result.isPresent()){
            newSetupName = result.get();
        }

        Boolean[][] cellsAsBool = Arrays.stream(window.getBoard().getCells())
                .map(cellLine ->
                        Arrays.stream(cellLine)
                                .map(cell -> cell.isAlive()).toArray(size -> new Boolean[size])
                ).toArray(size -> new Boolean[size][size]);

        String message = ExtractSetupDialogModel.generateCodeAsString(newSetupName,cellsAsBool);
        TextArea textArea = new TextArea(message);
        textArea.setEditable(false);
        textArea.setWrapText(true);
        GridPane gridPane = new GridPane();
        gridPane.setMaxWidth(Double.MAX_VALUE);
        gridPane.add(textArea, 0, 0);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(ExtractSetupDialogModel.TITLE2);
        alert.setHeaderText(ExtractSetupDialogModel.HEADER_TEXT2);

        alert.getDialogPane().setContent(gridPane);
        alert.showAndWait();
    }
}
