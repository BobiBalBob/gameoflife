package view.menu.listeners;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import view.Window;
import view.menu.SetupButton;


public class SetupButtonListener implements EventHandler<ActionEvent> {
    private SetupButton button;
    private Window window;

    public SetupButtonListener(SetupButton button, Window window) {
        this.button = button;
        this.window = window;
    }


    @Override
    public void handle(ActionEvent actionEvent) {
        window.loadSetup(button.getSetup());
    }
}
