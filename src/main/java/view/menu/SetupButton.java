package view.menu;

import javafx.scene.control.Button;
import model.GameConfiguration;
import view.Window;
import view.menu.listeners.SetupButtonListener;


public class SetupButton extends Button {
    private Boolean[][] setup;

    public SetupButton(String label, Boolean[][] setup, Window window) {
        super(label);
        this.setup = setup;
        this.setPrefWidth(GameConfiguration.BUTTON_SIZE);
        this.setOnAction(new SetupButtonListener(this, window));
    }


    public Boolean[][] getSetup() {
        return setup;
    }
}
