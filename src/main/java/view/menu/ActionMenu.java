package view.menu;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import model.GameConfiguration;
import view.Window;
import view.menu.listeners.ExtractButtonListener;


public class ActionMenu extends FlowPane {
    private Button startButton;
    private Button extractButton;
    private Window window;

    public ActionMenu(Window window) {
        this.window = window;
        setAlignment(Pos.CENTER);
        createStartButton();
        createExtractSetUpButton();
    }

    private void createStartButton() {
        startButton = new Button(GameConfiguration.START_BUTTON_LABEL);
        startButton.setPrefWidth(GameConfiguration.BUTTON_SIZE);
        startButton.setOnAction(actionEvent -> window.playStop());
        getChildren().add(startButton);
    }

    private void createExtractSetUpButton() {
        extractButton = new Button(GameConfiguration.EXTRACT_BUTTON_LABEL);
        extractButton.setPrefWidth(GameConfiguration.BUTTON_SIZE);

        extractButton.setOnAction(new ExtractButtonListener(window));
        getChildren().add(extractButton);
    }

    private void changeButtonStartLabel(boolean started) {
        if(started) {
            startButton.setText(GameConfiguration.STOP_BUTTON_LABEL);
        } else {
            startButton.setText(GameConfiguration.START_BUTTON_LABEL);
        }
    }

    public void stop() {
        changeButtonStartLabel(false);
        extractButton.setDisable(false);
    }

    public void start() {
        changeButtonStartLabel(true);
        extractButton.setDisable(true);
    }

}
