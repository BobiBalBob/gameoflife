package view.menu;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import model.GameConfiguration;
import view.Window;

public class GlobalMenu extends GridPane {
    private ActionMenu actionMenu;
    private SetupMenu setupMenu;
    private ConfigurationMenu configurationMenu;
    private Window window;
    private Label generationLabel;

    public GlobalMenu(Window window) {
        setAlignment(Pos.CENTER);
        this.window = window;

        actionMenu = new ActionMenu(window);
        setupMenu = new SetupMenu(window);
        configurationMenu = new ConfigurationMenu(window);
        generationLabel = new Label(GameConfiguration.GENERATION_LABEL + 0);

        this.add(generationLabel, 0, 0);
        this.add(actionMenu, 0, 0);
        this.add(setupMenu, 0, 1);
        this.add(configurationMenu, 0, 2);
    }
    public void updateGenerationLabel(int generation) {
        generationLabel.setText(GameConfiguration.GENERATION_LABEL + generation);
    }

    public void stopGame() {
        actionMenu.stop();
        setupMenu.disableButtons(false);
        configurationMenu.disableBoxSize(false);
    }

    public void startGame() {
        setupMenu.disableButtons(true);
        actionMenu.start();
        configurationMenu.disableBoxSize(true);
    }
    public Window getWindow() {
        return window;
    }
}
