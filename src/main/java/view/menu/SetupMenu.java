package view.menu;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import model.GameConfiguration;
import model.SetupFactory;
import view.Window;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SetupMenu extends FlowPane {
    private List<Button> setupButtons;
    private int buttonSize;

    public SetupMenu(Window window) {
        this.buttonSize = GameConfiguration.BUTTON_SIZE;
        setAlignment(Pos.CENTER);
        setMinWidth(buttonSize * 4);
        createSetupButton(window);
    }

    private void createSetupButton(Window window) {
        setupButtons = new ArrayList<>();
        for (Map.Entry<String,Boolean[][]> entry : SetupFactory.getSetupAsMap().entrySet()) {
            setupButtons.add(new SetupButton(entry.getKey(), entry.getValue(), window));
        }
        for(int i = 0; i< setupButtons.size(); i++) {
            getChildren().add(setupButtons.get(i));
        }
    }


    public void disableButtons(boolean disable) {
        setupButtons.stream().forEach(button -> button.setDisable(disable));
    }
}
