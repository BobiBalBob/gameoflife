package view.menu;

import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.FlowPane;
import model.GameConfiguration;
import view.Window;

public class ConfigurationMenu extends FlowPane {
    private ComboBox sizeCombo;

    private Window window;

    public ConfigurationMenu(Window window) {
        this.window = window;
        setAlignment(Pos.CENTER);
        createComboBox();
    }

    private void createComboBox() {
        ComboBox comboBoxSize = new ComboBox();
        sizeCombo = comboBoxSize;
        comboBoxSize.setPrefWidth(GameConfiguration.BUTTON_SIZE);
        comboBoxSize.getItems().addAll(GameConfiguration.SIZES_AVAILABLE);
        comboBoxSize.setPromptText(String.valueOf(window.getCellNumber()));
        comboBoxSize.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldvalue, Object newValue) -> {
            window.setBoardSize((Integer) newValue);
        });

        ComboBox comboBoxTime = new ComboBox();
        comboBoxTime.setPrefWidth(GameConfiguration.BUTTON_SIZE);
        comboBoxTime.getItems().addAll(GameConfiguration.SPEED_AVAILABLE);
        comboBoxTime.setPromptText(String.valueOf(window.getSpeed()));
        comboBoxTime.getSelectionModel().selectedItemProperty()
                .addListener((ObservableValue observable, Object oldvalue, Object newValue)
                        -> window.setSpeed((Integer) newValue)
                );

        ComboBox comboBoxSelectionSize = new ComboBox();
        comboBoxSelectionSize.setPrefWidth(GameConfiguration.BUTTON_SIZE);
        comboBoxSelectionSize.getItems().addAll(GameConfiguration.SELECTION_SIZE_AVAILABLE);
        comboBoxSelectionSize.setPromptText(String.valueOf(window.getSelectionSize()));
        comboBoxSelectionSize.getSelectionModel().selectedItemProperty()
                .addListener((ObservableValue observable, Object oldvalue, Object newValue)
                        -> window.setSelectionSize((Integer) newValue)
                );



        getChildren().add(comboBoxSize);
        getChildren().add(comboBoxTime);
        getChildren().add(comboBoxSelectionSize);
    }

    public void disableBoxSize(boolean disable) {
        sizeCombo.setDisable(disable);
    }
}
