import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import view.Window;

public class GameOfLifeMain extends Application {
    private Window window;

    @Override
    public void start(final Stage stage) {
        window = new Window();

        Scene scene = new Scene(window);
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}